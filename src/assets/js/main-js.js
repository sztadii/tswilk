$(document).ready(function () {

    windowWidth = window.innerWidth;

    shortText('.news-item > p', 80);
    shortText('.content-news-info > p', 120);

    addBackground();

    clickButton('#nav-top', '#menu-box');

    makeSliderMain();
    makeContentNewsSlider();

    moveSliderMain();
    moveBannerMain();
    moveSliderButtons();
    moveSliderButtons();

    setTimeout(function () {
        moveMagicLine('#menu-box', '#current_page_item', 'active', '#magic-line');
    }, 400);

    pulsAnimate('#question-left', 800, 0.3, 1);
    moveMagicLine();

});

$(window).resize(function () {

    windowWidth = window.innerWidth;

    moveSliderMain();
    moveBannerMain();
    moveSliderButtons();
    moveMagicLine('#menu-box', '#current_page_item', 'active', '#magic-line');

});

$(window).scroll(function () {

    screenBottom = $(window).scrollTop() + $(window).height();

    landingEachElements('.content-news-item .button1', '#content-news', 0.05, 200);

});
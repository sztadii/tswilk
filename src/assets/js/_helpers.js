//CLICKING FUNCTIONS region

function clickButton(_button, _object) {
    var button = $(_button),
        object = $(_object);

    if (button.length && object.length) {
        button.click(function () {
            object.slideToggle();
        });
    }
}

//endregion

//MAKING FUNCTIONS region

function makeSliderMain() {
    $('#slider-main').slick({
        dots: true,
        arrows: false,
        fade: true,
        infinite: true,
        speed: 500,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 3000,
        customPaging: function (slider, i) {
            return '<button type="button" data-role="none" role="button" aria-required="false" tabindex="0">0' + (i + 1) + '</button>';
        }
    });

    makeSliderArrows();
}

function makeSliderArrows() {
    var arrowNext = $('#arrow-next'),
        arrowPrev = $('#arrow-prev'),
        slider = $('#slider-main');

    arrowNext.click(function () {
        slider.slick('slickNext');
    });

    arrowPrev.click(function () {
        slider.slick('slickPrev');
    });
}

function makeContentNewsSlider() {
    $('#content-news').slick({
        arrows: false,
        dots: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        autoplay: true,
        vertical: true,
        cssEase: 'linear',
    });
}

//endregion

//MOVING FUNCTIONS region

function moveSliderMain() {
    if (windowWidth >= PC) {
        var position = ($(window).width() - 960) / 2;

        $('#slider-box').css({
            'left': '' + position + 'px'
        });
    } else {
        $('#slider-box').css({
            'left': 'auto'
        });
    }
}

function moveBannerMain() {
    if (windowWidth >= PC) {
        var position = ($(window).width() - 960) / 2;

        $('#banner-box').css({
            'left': '' + position + 'px'
        });
    } else {
        $('#banner-box').css({
            'left': 'auto'
        });
    }
}

function moveMagicLine(_menu, _currentPageItem, _active, _line) {
    var menuBox = $(_menu);

    if (windowWidth >= PC && menuBox.length) {

        if (!$(_line).length) {
            var tmp = _line.replace("#", "");

            if(_line[0] == "#")
                menuBox.append("<li id=" + tmp + "></li>");

            else if(_line[0] == ".")
                menuBox.append("<li class=" + tmp + "></li>");
        }

        var magicLine = $(_line),
            currentPageItem = $(_currentPageItem),
            currentPageAnchor = currentPageItem.find('a'),
            el, leftPos, newWidth;

        magicLine
            .width($(_currentPageItem).width())
            .css('left', currentPageAnchor.position().left)
            .data('origLeft', magicLine.position().left)
            .data('origWidth', magicLine.width());

        currentPageAnchor.addClass(_active);

        menuBox.find('li a').hover(function () {
            el = $(this);
            leftPos = el.position().left;
            newWidth = el.parent().width();

            magicLine.stop().animate({
                left: leftPos,
                width: newWidth
            });

            menuBox.find('a').removeClass(_active);
            el.addClass(_active);

        }, function () {
            magicLine.stop().animate({
                left: magicLine.data('origLeft'),
                width: magicLine.data('origWidth')
            });

            $(this).removeClass(_active);
            currentPageAnchor.addClass(_active);
        });
    } else if (windowWidth < PC && menuBox.length) {
        $(_line).remove();
    }
}

function moveSliderButtons() {
    var dots = $('#slider-main .slick-dots');

    if (windowWidth >= PC && dots.length) {
        var dotsHeight = dots.height(),
            dotsWidth = dots.width(),
            hollForButtonsHeight = 246,
            hollForButtonWidth = 98,
            spaceBeforeHoll = 130,
            dotsTopPosition =
                spaceBeforeHoll +
                ((hollForButtonsHeight - dotsHeight) / 2),
            dotsRightPosition =
                (hollForButtonWidth - dotsWidth) / 2;

        dots.css({
            'top': '' + dotsTopPosition + 'px',
            'right': '' + dotsRightPosition + 'px'
        });
    } else {
        dots.css({
            'top': 'auto',
            'right': 'auto'
        });
    }
}

//endregion

//LANDING FUNCTION region

function landingElement(_object, _container, _height) {
    var object1 = $(_object),
        container = $(_container),
        height = _height;

    if (object1.length && windowWidth >= PC) {
        var triggerPosition = container.offset().top + container.outerHeight() * height;

        if (screenBottom > triggerPosition) {
            object1.addClass('showing');
        }
    }
}

function landingEachElements(_objects, _container, _height, _time) {
    var objects = $(_objects),
        container = $(_container),
        height = _height,
        time = _time;

    if (objects.length && windowWidth >= PC) {
        var triggerPosition = container.offset().top + container.outerHeight() * height;

        if (screenBottom > triggerPosition) {
            objects.each(function (i) {
                setTimeout(function () {
                    objects.eq(i).addClass('showing');
                }, time * (i + 1));
            });
        }
    }
}

//endregion

//FADING FUNCTIONS region

function fadingElement(_object) {
    var object = $(_object);

    if (object.length && windowWidth >= PC) {
        var wScroll = $(window).scrollTop(),
            value = 100 / (wScroll + 20);


        object.css({
            'opacity': value <= 1 ? value : 1
        });


    } else {
        object.css({
            'opacity': '1'
        });

    }
}

function pulsAnimate(_object, time, start, end) {

    var object = $(_object);

    if (object.length) {
        makePuls();

        function makePuls() {
            $(object).animate({
                'opacity': '' + start + ''
            }, time, function () {
                $(this).animate({
                    'opacity': '' + end + ''
                }, time, makePuls);
            });
        }
    }

}

//endregion

//ADDING FUNCTIONS region

function addBackground() {
    var background = $('#pc-background-main');

    if($('#banner-box').length) {
        background.attr('id', 'pc-background-secondary');
    }
}

//endregion

//TEXT FUNCTIONS region

function shortText(_object, _maxLength) {
    var object = $(_object);

    if (object.length) {
        object.each(function () {
            var element = $(this),
                elementText = element.text().trim(),
                shortText;

            if (elementText.length > _maxLength) {

                shortText = elementText.substr(0, _maxLength);

                var arg1 = _maxLength,
                    arg2 = shortText.lastIndexOf(" "),
                    mathMin = Math.min(arg1, arg2);

                if (isSpacecialCharacter(shortText[mathMin - 1]))
                    --mathMin;

                shortText = shortText.substr(0, mathMin) + '...';
            }

            else {
                shortText = elementText.substr(0, _maxLength);
            }

            element.text(shortText);
        });
    }
}

function isSpacecialCharacter(_character) {
    if (/^[a-zA-Z0-9- ]*$/.test(_character) == false) {
        return true;
    }

    return false;
}

//endregion

